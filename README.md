# firestorm-ua



## Getting started

EN
Welcome to Firestorm Viewer Ukrainian localization project!

The goal of project is to implement Ukrainian language as an option in settings of Firestorm Viewer.
This is really raw version of it, though you can install it and help us to find some errors, or even fix them with us. For those who are interested - there is an instruction below.

    1. Download entire project by clicking Code -> Download source code -> zip.
    2. Unpack everything with replacement in Firestorm-Releasex64 folder, by default for Windows it's C:\Program Files\Firestorm-Releasex64. 
    For example, "app_settings" folder from firestorm-ua.zip replaces same folder from Firestorm-Releasex64 folder.
    3. When you start Firestorm-Releasex64, simply open settings, and check for Ukrainian language. 
    If it exists - click and select it, then restart Firestorm-Releasex64. 
    If Ukrainian language didn't show up - please do all the steps again.
    4. Be happy with result.

UA
Ласкаво просимо до проєкту локалізації Firestorm Viewer українською мовою!

Мета проєкту — додати українську мову як опцію в налаштування Firestorm Viewer. Це дуже чернеткова версія, але ви можете встановити її та допомогти нам знайти помилки або навіть виправити їх разом із нами. Для зацікавлених – нижче наведено інструкцію.

   1. Завантажте весь проєкт, натиснувши Code -> Download source code -> zip.
   2. Розпакуйте всі файли з заміною в папку Firestorm-Releasex64, за замовчуванням для Windows це C:\Program Files\Firestorm-Releasex64.
      Наприклад, папка "app_settings" з firestorm-ua.zip замінює таку ж папку у Firestorm-Releasex64.
   3. Після запуску Firestorm-Releasex64 просто відкрийте налаштування та перевірте наявність української мови.
      Якщо вона з'явилась – оберіть її та перезапустіть Firestorm-Releasex64.
      Якщо українська мова не відображається – повторіть усі кроки заново.
   4. Насолоджуйтесь результатом.

RU
Добро пожаловать в проект локализации Firestorm Viewer на украинский язык!

Цель проекта – добавить украинский язык в настройки Firestorm Viewer в качестве опции. Сейчас это очень черновая версия, но вы можете установить её и помочь нам найти ошибки или даже исправить их вместе с нами. Для заинтересованных - ниже приведена инструкция.

   1. Скачайте весь проект, нажав Code -> Download source code -> zip.
   2. Распакуйте все файлы с заменой в папку Firestorm-Releasex64, по умолчанию для Windows это C:\Program Files\Firestorm-Releasex64.
      Например, папка "app_settings" из firestorm-ua.zip заменяет такую же папку в Firestorm-Releasex64.
   3. При запуске Firestorm-Releasex64 просто откройте настройки и проверьте наличие украинского языка.
      Если он появился – выберите его и перезапустите Firestorm-Releasex64.
      Если украинский язык не отображается – повторите все шаги заново.
   4. Наслаждайтесь результатом.

